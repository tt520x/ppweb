#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import jinja2
import argparse

def render(j2_path, context):
  path, filename = os.path.split(j2_path)
  return jinja2.Environment(
           loader=jinja2.FileSystemLoader(path or './')
         ).get_template(filename).render(context)

parser = argparse.ArgumentParser()

parser.add_argument("--src", type=str,
                    help="This option should set the path of jinja2 template")
parser.add_argument("--dist", type=str,
                    help="This option should set the path of distribution that you wanted")

args = parser.parse_args()

context={}

if  not args.src:
  raise Exception("The 'src' option is required")

if  not args.dist:
  raise Exception("The 'dist' option is required")

try:

  for item in os.environ:
    context[item]=os.getenv(item)

  result = render(args.src, context)
  with open(args.dist, "w") as w:
    w.write(result)
except:
  raise Exception("Something error occured.")
